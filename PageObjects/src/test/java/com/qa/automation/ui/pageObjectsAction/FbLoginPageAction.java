package com.qa.automation.ui.pageObjectsAction;

import com.qa.automation.ui.pageObjects.FbLoginPage;
import org.openqa.selenium.WebDriver;

public class FbLoginPageAction extends FbLoginPage {

    public FbLoginPageAction(WebDriver webDriver) {
        super(webDriver);
    }


    public void inputEmail(String email) {
        inputEmailField(email);
    }

    public void inputPass(String pass) {
        inputPasswordField(pass);
    }

    public void clickLogin(String btnName) {
        clickLoginButton(btnName);
    }
}
