package com.qa.automation.ui.pageObjects;

import com.qa.automation.ui.WebTestDriver;
import org.openqa.selenium.WebDriver;

public class FbLoginPage {
    WebTestDriver webTestDriver;


    private static final String EMAIL_FIELD = "//form[contains(@class, 'formContainer')]//input[@id='email']";
    private static final String PASSWORD_FIELD = "//form[contains(@class, 'formContainer')]//input[@id='pass']";
    private static final String LOGIN_BUTTON = "//form[contains(@class, 'formContainer')]//button[@name='%s']";


    public FbLoginPage(WebDriver webDriver) {
        this.webTestDriver = new WebTestDriver(webDriver);
    }

    public void inputEmailField(String email) {
        webTestDriver.inputText(EMAIL_FIELD, email);
    }

    public void inputPasswordField(String pass) {
        webTestDriver.inputText(PASSWORD_FIELD, pass);
    }

    public void clickLoginButton(String btnName) {
        String loginButton = String.format(LOGIN_BUTTON, btnName);
        webTestDriver.clickControl(loginButton);
    }

}
