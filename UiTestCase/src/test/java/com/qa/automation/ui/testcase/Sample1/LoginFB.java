package com.qa.automation.ui.testcase.Sample1;

import com.qa.automation.ui.pageObjectsAction.FbLoginPageAction;
import com.qa.automation.ui.testcase.UiTestCaseBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginFB extends UiTestCaseBase {
    public FbLoginPageAction fbLoginPageAction;
    private String url = "https://www.facebook.com";

    @BeforeClass
    public void setup() {
        fbLoginPageAction = new FbLoginPageAction(getDriver());
    }

    @Test
    public void navigateFbPage() {
        browseUrl(url);
    }

    @Test(dependsOnMethods = {"navigateFbPage"})
    public void fillEmailInfo() {
        fbLoginPageAction.inputEmail("");
    }

    @Test(dependsOnMethods = {"fillEmailInfo"})
    public void fillPasswordInfo() {
        fbLoginPageAction.inputPass("");
    }

    @Test(dependsOnMethods = {"fillPasswordInfo"})
    public void clickOnLoginButton() {
        fbLoginPageAction.clickLogin("login");
    }
}
