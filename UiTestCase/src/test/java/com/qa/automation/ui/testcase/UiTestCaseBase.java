package com.qa.automation.ui.testcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;


public class UiTestCaseBase {
    public WebDriver driver;

    @BeforeSuite
    public void init() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nbl24\\.m2\\repository\\webdriver\\chromedriver\\win32\\86.0.4240.75\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    public void browseUrl(String url) {
        driver.get(url);
    }

    public WebDriver getDriver() {
        return driver;
    }
}
